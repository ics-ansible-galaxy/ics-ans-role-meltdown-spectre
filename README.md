ics-ans-role-meltdown-spectre
===================

Ansible role to install meltdown-spectre.
It checks hosts using the spectre-meltdown-checker from speed47.
https://github.com/speed47/spectre-meltdown-checker.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.19

Role Variables
--------------
None, however tags can be included to limit checks.
For example:

`--tags=spectre`
A full list of tasks are:

- variant-1
- variant-2
- variant-3
- meltdown
- spectre

```yaml
...
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-meltdown-spectre
```

License
-------

BSD 2-clause
