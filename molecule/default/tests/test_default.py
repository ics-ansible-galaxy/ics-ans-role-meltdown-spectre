import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_spectre_meltdown_checker_exists(host):
    assert host.file("/opt/spectre-meltdown-checker/spectre-meltdown-checker.sh").exists
